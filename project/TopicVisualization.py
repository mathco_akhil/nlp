import pandas as pd
import pyLDAvis
import pyLDAvis.sklearn
import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS

class TopicVisualization(object):
    """
    This class contains several functions for displaying the topics with words associated with it.
    """
        
    def __init__(self,modelOutput,n_components,matrix,vectorizer):
        """
        The constructor of the class creates a TopicVisualization object
        Parameters:
        -----------
        modelOutput  : lda/lsa/nmf model output 
        n_components : number of topics which the user want to generate.
        matrix       : sparse matrix which is used from the vectorizeBOW/vectorizeTfidf function while building the model
        vectorizer   : CountVectorizer/tfidfvectorizer object which is used while building the object.
        
        Examples:
        ---------
        topicVisObj = TopicVisualization(ldaObject.lda,ldaObject.n_components,ldaObject.matrix,ldaObject.vectorizer)
        """
        self.modelOutput = modelOutput
        self.n_components = n_components
        self.matrix = matrix
        self.vectorizer = vectorizer          

    def displayTopic(self,numberOfTopWords=40):
        """
        This function generates a dataframe which contains topics with its associated words.
    
        Parameters:
        -----------
        numberOfTopWords    : Number of words per topic to generate in a dataframe
    
        Examples:
        ---------        
        topicWordsDf = topicVisObj.displayTopic(numberOfTopWords=40)
        
        """
        model = self.modelOutput
        vectorizer = self.vectorizer 
        featureNames = vectorizer.get_feature_names()
        nTopics=self.n_components
        wordDict={}
        df=pd.DataFrame()
        for i in range(nTopics):
            wordsIds = model.components_[i].argsort()[:-numberOfTopWords - 1:-1]
            words = [featureNames[key] for key in wordsIds]
            wordDict['Topic # ' + '{:02d}'.format(i+1)] = words
        df = pd.DataFrame(wordDict)
        return df
    
   
    def visualizeTopics(self,mds='tsne',modelType=None,R=30, lambda_step=0.01,n_jobs=-1, plot_opts={'xlab': 'PC1', 'ylab': 'PC2'}, sort_topics=True):
        """
        This function generates a html file using pyLDAvis package which helps in interpreting the topics with its words
        This method generates output only for model build by LDA and NMF. LSA model is not applicable.
    
        Parameters:
        -----------
        mds : function or a string representation of function  A string representation currently accepts pcoa (or upper case variant), mmds (or upper case variant) and tsne (or upper case variant), if sklearn package is installed for the latter two. 
        modelType : 'lda' | 'nmf' 
        R : The number of terms to display in the barcharts of the visualization. Default is 30. Recommended to be roughly between 10 and 50. 
        lambda_step : float, between 0 and 1. Default is 0.01 Determines the interstep distance in the grid of lambda values over which to iterate when computing relevance.Recommended to be between 0.01 and 0.1.
        n_jobs : The number of cores to be used to do the computations. The regular joblib conventions are followed so -1, which is the default, will use all cores. 
        plot_opts :  dict, with keys 'xlab' and ylab Dictionary of plotting options, right now only used for the axis labels.
        sort_topics : sort topics by topic proportion (percentage of tokens covered). Set to false to keep original topic order 

        Examples:
        ---------        
        topicVisObj.visualizeTopics(mds='tsne',modelType='lda')
        
        """
        model = self.modelOutput
        sparseVectorizedMatrix=self.matrix
        vectorizer=self.vectorizer
        pyLDAvis.enable_notebook()
        panel = pyLDAvis.sklearn.prepare(model, sparseVectorizedMatrix, vectorizer, mds=mds,R=R, lambda_step=lambda_step,n_jobs=n_jobs, plot_opts=plot_opts, sort_topics=sort_topics)
        fileType= ".html"
        pyLDAvis.save_html(panel,modelType+fileType)
         
         
    def generateWordCloudObject(self,numberOfTopWords=40,font_path=None, width=400, height=200, margin=2, ranks_only=None, prefer_horizontal=0.9, mask=None, scale=1, color_func=None, max_words=200, min_font_size=4, stopwords=None, random_state=None, background_color='black', max_font_size=None, font_step=1, mode='RGB', relative_scaling='auto', regexp=None, collocations=True, colormap=None, normalize_plurals=True, contour_width=0, contour_color='black', repeat=False):
        """
        This function creates word cloud object for all the topics with its words.
    
        Parameters:
        -----------
        numberOfTopWords : Number of words required per topic to display in the word cloud image 
        font_path        : string ,Font path to the font that will be used (OTF or TTF). 
        width            : int (default=400) Width of the canvas.
        height           : int (default=200) Height of the canvas.
        prefer_horizontal: float (default=0.90) The ratio of times to try horizontal fitting as opposed to vertical. If prefer_horizontal < 1, the algorithm will try rotating the word if it doesn’t fit. 
        mask             : nd-array or None (default=None) If not None, gives a binary mask on where to draw words. 
        contour_width    : float (default=0) If mask is not None and contour_width > 0, draw the mask contour.
        contour_color    : color value (default=”black”) Mask contour color.
        scale            : float (default=1) Scaling between computation and drawing. 
        min_font_size    : int (default=4) Smallest font size to use. Will stop when there is no more room in this size.
        font_step        : int (default=1) Step size for the font. font_step > 1 might speed up computation but give a worse fit.
        max_words        : number (default=200) The maximum number of words.
        stopwords        : set of strings or None The words that will be eliminated. If None, the build-in STOPWORDS list will be used.
        background_color : color value (default="black")
        max_font_size    : int or None (default=None) Maximum font size for the largest word. If None, height of the image is used.
        mode             : string (default="RGB") Transparent background will be generated when mode is "RGBA" and background_color is None.
        relative_scaling : float (default='auto') Importance of relative word frequencies for font-size. With relative_scaling=0, only word-ranks are considered. With relative_scaling=1, a word that is twice as frequent will have twice the size. If you want to consider the word frequencies and not only their rank, relative_scaling around .5 often looks good. If 'auto' it will be set to 0.5 unless repeat is true, in which case it will be set to 0.
        collocations     : bool, default=True Whether to include collocations (bigrams) of two words. Ignored if using generate_from_frequencies.
        colormap         : string or matplotlib colormap, default="viridis" Matplotlib colormap to randomly draw colors from for each word. Ignored if “color_func” is specified.
        normalize_plurals: bool, default=True Whether to remove trailing 's' from words.
        repeat           : bool, default=False Whether to repeat words and phrases until max_words or min_font_size is reached.

        Examples:
        ---------        
        wordCloudObjectList = topicVisObj.generateWordCloudObject(numberOfTopWords=40,font_path=None, width=400, height=200, margin=2, ranks_only=None, prefer_horizontal=0.9, mask=None, scale=1, color_func=None, max_words=200, min_font_size=4, stopwords=None, random_state=None, background_color='black', max_font_size=None, font_step=1, mode='RGB', relative_scaling='auto', regexp=None, collocations=True, colormap=None, normalize_plurals=True, contour_width=0, contour_color='black', repeat=False)
        
        """
        nTopics=self.n_components
        model=self.modelOutput
        vectorizer=self.vectorizer
        featureNames = vectorizer.get_feature_names()
        words=[]
        wordCloudObjectList = []
        for i in range(nTopics):
            wordsIds = model.components_[i].argsort()[:-numberOfTopWords - 1:-1]
            words = [featureNames[key] for key in wordsIds]
            sentence =" ".join(words)
            cloud = WordCloud(font_path=font_path, width=width, height=height, margin=margin, ranks_only=ranks_only, prefer_horizontal=prefer_horizontal, mask=mask, scale=scale, color_func=color_func, max_words=max_words, min_font_size=min_font_size, stopwords=stopwords, random_state=random_state, background_color=background_color, max_font_size=max_font_size, font_step=font_step, mode=mode, relative_scaling=relative_scaling, regexp=regexp, collocations=collocations, colormap=colormap, normalize_plurals=normalize_plurals, contour_width=contour_width, contour_color=contour_color, repeat=repeat).generate(sentence)
            wordCloudObjectList.append(cloud)
        return wordCloudObjectList
            

            
    def plotWordCloud(self,wordCloudObjectList):
        """
        This function generates word cloud images for all the topics with its words from the word cloud object list.
    
        Parameters:
        -----------
        wordCloudObjectList : contains a list of word cloud objects from which word cloud images are generated
        
        Examples:
        ---------        
        topicVisObj.plotWordCloud(wordCloudObjectList)
                
        """
        i = 0
        for wordCloudImage in wordCloudObjectList:
            i = i+1
            plt.title("topic_"+str(i))
            plt.imshow(wordCloudImage)
            plt.axis('off')
            plt.show()
            
        
        
    