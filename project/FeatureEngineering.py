import pandas as pd
import nltk
# ntk.download('wordnet')
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from gensim.models.doc2vec import TaggedDocument, Doc2Vec
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np


@pd.api.extensions.register_dataframe_accessor("featureEngineering")
@pd.api.extensions.register_series_accessor("featureEngineering")
class FeatureEngineering(object):
    """
    This class contains methods required to process text data in order to perform different NLP techniques
    """
    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
        
    def vectorizeBOW(self,colName, strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), analyzer='word', max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False):
        """
        vectorizeBOW - This function uses CountVectorizer function of scikit-learn to produce bag of words,
        which is used to extract features from text documents. These features can be used for modelling.
        It converts a collection of text documents(tokenized words) to a sparsed matrix of token counts. 
        It produces a sparse representation of counts using scipy.sparse.csr_matrix.
    
        Parameters:
        -----------
        colName        : Column name of tokenized words which need to be convert to vectors using bag of words.
        strip_accents  :  {'ascii', unicode', None} Remove accents and perform other character normalization during the preprocessing step. 
        lowercase      :  boolean, True by default. Converts all characters to lower case. User can set to False as this process has been done in the text cleaning step.
        preprocessor   :  callable or None (default). Override the preprocessing (string transformation) stage while preserving the tokenizing and n-grams generation steps.
        tokenizer      :  callable or None (default). Override the string tokenization step while preserving the preprocessing and n-grams generation steps. Only applies if analyzer == 'word'. 
        stop_words     :  string {‘english’}, list, or None (default). If a list, that list is assumed to contain stop words, all of which will be removed from the resulting tokens. Only applies if analyzer == 'word'.
        token_pattern  :   string.  Regular expression denoting what constitutes a “token”, only used if analyzer == 'word'.If column is tokenized earlier, then no need to give the pattern. 
        ngram_range    : tuple (min_n, max_n) The lower and upper boundary of the range of n-values for different n-grams to be extracted. All values of n such that min_n <= n <= max_n will be used.
        analyzer       :  string, {‘word’, ‘char’, ‘char_wb’} or callable.  Whether the feature should be made of word or character n-grams. Option ‘char_wb’ creates character n-grams only from text inside word boundaries 
        max_df         :   float in range [0.0, 1.0] or int, default=1.0 . When building the vocabulary ignore terms that have a document frequency strictly higher than the given threshold 
        min_df         :   float in range [0.0, 1.0] or int, default=1  When building the vocabulary ignore terms that have a document frequency strictly lower than the given threshold. 
        max_features   :   int or None, default=None  If not None, build a vocabulary that only consider the top max_features ordered by term frequency across the corpus. 
        vocabulary     :   Mapping or iterable, optional Either a Mapping (e.g., a dict) where keys are terms and values are indices in the feature matrix, or an iterable over terms. If not given, a vocabulary is determined from the input documents. 
        binary         :  boolean, default=False  If True, all non zero counts are set to 1. 
    
        Examples:
        ---------
        bow = df.featureEngineering.vectorizeBOW(colName='Tokenized_tweet', strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), analyzer='word', max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False)
        >>> Output: A tuple is generated-> (sparse.csr.csr_matrix,feature_extraction.text.CountVectorizer)
        bow[0] = sparsed_matrix
        bow[1] = CountVectorizer object
    
        """
        df = self._obj.copy()
        tupleOutput=()
        corpus=[]
        for k in df[colName]:
            sentence = " ".join(r for r in k)
            corpus.append(sentence)
        vectorizer = CountVectorizer(strip_accents=strip_accents, lowercase=lowercase, preprocessor=preprocessor, tokenizer=tokenizer, stop_words=stop_words, token_pattern=token_pattern, ngram_range=ngram_range, analyzer=analyzer, max_df=max_df, min_df=min_df, max_features=max_features, vocabulary=vocabulary, binary=binary)
        matrix = vectorizer.fit_transform(corpus)
        tupleOutput=(matrix,vectorizer)

        return tupleOutput
    
    
    

    def vectorizeHashing(self,colName,strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), analyzer='word', n_features=1048576, binary=False, norm='l2', alternate_sign=True):
        """
        vectorizeHashing - This function converts a collection of text documents(in the form of tokenized words) to a matrix of token occurences.
        HashingVectorizer function of scikit-learn implements this approach that can be used to consistently hash words, then tokenize and encode documents as needed.

        Parameters:
        -----------
        colName        : Column name of tokenized words which need to be convert to vectors using hashing vectorizer technique.
        strip_accents  :  {'ascii', unicode', None} 
        lowercase      :  boolean, True by default. 
        preprocessor   :  callable or None (default). Override the preprocessing (string transformation) stage while preserving the tokenizing and n-grams generation steps.
        tokenizer      :  callable or None (default). Override the string tokenization step while preserving the preprocessing and n-grams generation steps. Only applies if analyzer == 'word'. 
        stop_words     :  string {‘english’}, list, or None (default). If a list, that list is assumed to contain stop words, all of which will be removed from the resulting tokens. Only applies if analyzer == 'word'.
        token_pattern  :   string  Regular expression denoting what constitutes a “token”, only used if analyzer == 'word'.If column is tokenized, then no need to give the pattern. 
        ngram_range    : tuple (min_n, max_n) The lower and upper boundary of the range of n-values for different n-grams to be extracted. All values of n such that min_n <= n <= max_n will be used.
        analyzer       :  string, {‘word’, ‘char’, ‘char_wb’} or callable  Whether the feature should be made of word or character n-grams. Option ‘char_wb’ creates character n-grams only from text inside word boundaries 
        n_features     : integer, default=(2 ** 20) The number of features (columns) in the output matrices. 
        binary         : boolean, default=False. If True, all non zero counts are set to 1 
        norm           : 'l1', 'l2' or None, optional Norm used to normalize term vectors. None for no normalization.
        alternate_sign : boolean, optional, default True When True, an alternating sign is added to the features as to approximately conserve the inner product in the hashed space even for small n_features. This approach is similar to sparse random projection.

            
        Examples:
        ---------
        hashing_matrix = df.featureEngineering.vectorizeHashing(colName='Tokenized_tweet',strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), analyzer='word', n_features=1048576, binary=False, norm='l2', alternate_sign=True)
        
        >>> Output: A hashing matrix is generated which can be given as a direct input to the model for text classification

        """
        df = self._obj.copy()
        corpus=[]
        for k in df[colName]:
            sentence = " ".join(r for r in k)
            corpus.append(sentence)
        vectorizer = HashingVectorizer(strip_accents=strip_accents, lowercase=lowercase, preprocessor=preprocessor, tokenizer=tokenizer, stop_words=stop_words, token_pattern=token_pattern, ngram_range=ngram_range, analyzer=analyzer, n_features=n_features, binary=binary, norm=norm, alternate_sign=alternate_sign)
        matrix = vectorizer.fit_transform(corpus)
        return matrix
    
        
    
    def embedDoc2Vec(self, colName, vector_size = 7, min_count = 0):
        """
        Run Doc2Vec embedding technique on tokenized data and return list of vectors with defined size.
        
        Parameters:
        -----------
        colName        : Column name of tokenized words needed to vectorize.
        vector_size    : Dimensionality of the feature vectors. 
        min_count      : Ignores all words with total frequency lower than this.
        
        Examples:
        ---------
        df = df.featureEngineering.embedDoc2Vec(colName='Tokenized_tweet')
        """
        df = self._obj
        tagged = [TaggedDocument(words, ['d{}'.format(idx)]) for idx, words in enumerate(df[colName])]
        model = Doc2Vec(tagged, vector_size=vector_size, min_count=min_count)
        return model.docvecs
    
    
            
        
    def vectorizeTfidf(self,colName,strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, analyzer='word', stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False, norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=False):
        """
        vectorizeTfidf - This function uses TfidfVectorizer function of scikit-learn 
        on the raw data(in the form of tokenized words) to produce features using TF-IDF technique,
        These features can be used for modelling.
    
        Parameters:
        -----------
        colName        : Column name of tokenized words which need to be convert to vectors using TF-IDF.
        strip_accents  :  {'ascii', unicode', None} 
        lowercase      :  boolean, True by default. 
        preprocessor   :  callable or None (default). Override the preprocessing (string transformation) stage while preserving the tokenizing and n-grams generation steps.
        tokenizer      :  callable or None (default). Override the string tokenization step while preserving the preprocessing and n-grams generation steps. Only applies if analyzer == 'word'. 
        stop_words     :  string {‘english’}, list, or None (default). If a list, that list is assumed to contain stop words, all of which will be removed from the resulting tokens. Only applies if analyzer == 'word'.
        token_pattern  :   string  Regular expression denoting what constitutes a “token”, only used if analyzer == 'word'.If column is tokenized, then no need to give the pattern. 
        ngram_range    : tuple (min_n, max_n) (default=(1, 1)) The lower and upper boundary of the range of n-values for different n-grams to be extracted. All values of n such that min_n <= n <= max_n will be used.
        analyzer       :  string, {‘word’, ‘char’, ‘char_wb’} or callable  Whether the feature should be made of word or character n-grams. Option ‘char_wb’ creates character n-grams only from text inside word boundaries 
        max_df         :   float in range [0.0, 1.0] or int, default=1.0  When building the vocabulary ignore terms that have a document frequency strictly higher than the given threshold 
        min_df         :   float in range [0.0, 1.0] or int, default=1  When building the vocabulary ignore terms that have a document frequency strictly lower than the given threshold. 
        max_features   :   int or None, default=None  If not None, build a vocabulary that only consider the top max_features ordered by term frequency across the corpus. 
        vocabulary     :   Mapping or iterable, optional Either a Mapping (e.g., a dict) where keys are terms and values are indices in the feature matrix, or an iterable over terms. If not given, a vocabulary is determined from the input documents. 
        binary         :  boolean, default=False  If True, all non zero counts are set to 1. 
        norm           : 'l1', 'l2' or None, optional (default='l2') Each output row will have unit norm, either: * ‘l2’: Sum of squares of vector elements is 1. The cosine similarity between two vectors is their dot product when l2 norm has been applied. * ‘l1’: Sum of absolute values of vector elements is 1
        use_idf        : boolean (default=True) Enable inverse-document-frequency reweighting.
        smooth_idf     : boolean (default=True) Smooth idf weights by adding one to document frequencies, as if an extra document was seen containing every term in the collection exactly once. Prevents zero divisions.
        sublinear_tf   : boolean (default=False) Apply sublinear tf scaling, i.e. replace tf with 1 + log(tf).

            
        Examples:
        ---------
        tfidf = df.featureEngineering.vectorizeTfidf(colName='Tokenized_tweet',strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, analyzer='word', stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False, norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=False)
        >>> Output: A tuple is generated-> (sparse.csr.csr_matrix,feature_extraction.text.TfidfVectorizer)
        tfidf[0] = sparsed_matrix
        tfidf[1] = TfidfVectorizer object
    
        """

        df = self._obj.copy()
        tupleOutput=()
        corpus=[]
        for k in df[colName]:
            sentence = " ".join(r for r in k)
            corpus.append(sentence)
        vectorizer = TfidfVectorizer(strip_accents=strip_accents, lowercase=lowercase, preprocessor=preprocessor, tokenizer=tokenizer, analyzer=analyzer, stop_words=stop_words, token_pattern=token_pattern, ngram_range=ngram_range, max_df=max_df, min_df=min_df, max_features=max_features, vocabulary=vocabulary, binary=binary, norm=norm, use_idf=use_idf, smooth_idf=smooth_idf, sublinear_tf=sublinear_tf)
        matrix = vectorizer.fit_transform(corpus)
        tupleOutput=(matrix,vectorizer)

        return tupleOutput
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
