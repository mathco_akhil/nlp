# -*- coding: utf-8 -*-
import pandas as pd
#from nltk.stem.porter import PorterStemmer
import string
import re
import nltk


stopword = nltk.corpus.stopwords.words('english')

df = pd.read_csv('clean_dataset.csv')
#df.columns = ['id','tweet']
df['tweet'] = df['tweet'].apply(str)

def clean_text(text):
    text_lc = "".join([word.lower() for word in text if word not in string.punctuation]) # remove puntuation
    text_rc = re.sub('[0-9]+', '', text_lc)
    tokens = re.split('\W+', text_rc)    # tokenization
    text = [word for word in tokens if word not in stopword]  # remove stopwords and stemming
    return text

df['tweet'] = df['tweet'].apply(lambda x: clean_text(x))


###########################################################################################################
import pandas as pd
df = pd.read_csv('clean_dataset.csv')
#pd.set_option('display.expand_frame_repr', False)
df = df[['label','tweet']]
df['tweet'] = df['tweet'].astype(str)
df = df.textPreProcessing.tokenization(['tweet'])
df = df[['label','Tokenized tweet']]

a = df.featureEngineering.vectorizeTfidf('Tokenized tweet')
x=a[0]
from sklearn.linear_model import LogisticRegression
log = LogisticRegression()