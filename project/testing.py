import FeatureEngineering
import TextPreProcessing
import pandas as pd

df = pd.read_csv(r"clean_dataset_v2.csv")

df = df.textPreProcessing.tokenization(['tweet'])
df['tweet']

vectorizer = TfidfVectorizer(strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, analyzer='word', stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False, norm='l2', use_idf=True, smooth_idf=True, sublinear_tf=False)
corpus=[]
for k in df['Tokenized tweet']:
    string1 = " ".join(r for r in k)
    corpus.append(string1)
fit = vectorizer.fit(corpus)
fit.todok().items()
matrix = fit.transform()


tfIdf = df.featureEngineering.vectorizeTfidf('Tokenized tweet')
tfIdf[0]

f= open("vectorizeTfidf.txt","w+")
f.write(str(tfIdf[0].todok().items()))
f.close()


# str(bowVec[0].todok().items())