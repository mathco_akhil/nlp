import unittest
from unittest.mock import patch
import pandas as pd
import sys
import os
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import TextPreProcessing
import FeatureEngineering



class TestFeatureEngineering(unittest.TestCase):

    def setUp(self):
        self.dfRaw = pd.read_csv(r"clean_dataset_v2.csv")
        self.dfTokenize = self.dfRaw.textPreProcessing.tokenization(colNames=['tweet'])

        
    def test_vectorizeBOW(self):
        dfBowVec = self.dfTokenize.copy()
        bowVec = dfBowVec.featureEngineering.vectorizeBOW('Tokenized_tweet')
        f= open("vectorizeBOW.txt","w+")
        bowVecContent = f.read()
        f.close()
        pd.util.testing.equalContents(str(bowVec[0].todok().items())+str(bowVec[1]), bowVecContent)

    def test_vectorizeHashing(self):
        dfHashVec = self.dfTokenize.copy()
        hashVec = dfHashVec.featureEngineering.vectorizeHashing('Tokenized_tweet')
        f= open("vectorizeHashing.txt","w+")
        hashVecContent = f.read()
        f.close()
        pd.util.testing.equalContents(str(hashVec.todok().items()), hashVecContent)

    def test_embedDoc2Vec(self):
        dfembedVec = self.dfTokenize.copy()
        embedVec = dfembedVec.featureEngineering.embedDoc2Vec('Tokenized_tweet')
        f= open("embedDoc2Vec.txt","w+")
        embedContent = f.read()
        f.close()
        pd.util.testing.equalContents(str(embedVec.doctags.items()), str(embedContent))

    def test_vectorizeTfidf(self):
        dftfIdf = self.dfTokenize.copy()
        tfIdf = dftfIdf.featureEngineering.vectorizeTfidf('Tokenized_tweet')
        f= open("vectorize.txt","w+")
        tfIdfContent = f.read()
        f.close()
        pd.util.testing.equalContents(str(tfIdf[0].todok().items()), tfIdfContent)