import unittest
from TextPreProcessing_test import TestTextPreProcessing
from NaiveBayes_test import TestNaiveBayes
from RandomForestClassifier_test import TestRandomForest
from FeatureEngineering_test import TestFeatureEngineering
from SVM_test import TestSVM
from Logistic_test import TestLogisticMethods
from gradientBoosting_test import TestgradientBoostingMethods
from TextClustering_test import TestTextClustering



def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestTextPreProcessing))
#    suite.addTest(unittest.makeSuite(TestNaiveBayes))
#    suite.addTest(unittest.makeSuite(TestRandomForest))
    suite.addTest(unittest.makeSuite(TestFeatureEngineering))
#    suite.addTest(unittest.makeSuite(TestSVM))
#     suite.addTest(unittest.makeSuite(TestLogisticMethods))
    suite.addTest(unittest.makeSuite(TestNaiveBayes))
    suite.addTest(unittest.makeSuite(TestRandomForest))
#    suite.addTest(unittest.makeSuite(TestFeatureEngineering))
    suite.addTest(unittest.makeSuite(TestSVM))
#   suite.addTest(unittest.makeSuite(TestLogisticMethods))
#    suite.addTest(unittest.makeSuite(TestgradientBoostingMethods))
#    suite.addTest(unittest.makeSuite(TestSVM))
    suite.addTest(unittest.makeSuite(TestLogisticMethods))
    suite.addTest(unittest.makeSuite(TestgradientBoostingMethods))
    suite.addTest(unittest.makeSuite(TestTextClustering))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())