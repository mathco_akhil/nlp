import pandas as pd
df = pd.read_csv("D:/MathMarket/NLP/nlp/project/clean_dataset_v2.csv")
df = df.textPreProcessing.tokenization(["tweet"],by='Word')
bow = df.featureEngineering.vectorizeBOW(colName='Tokenized_tweet', strip_accents=None, lowercase=True, preprocessor=None, tokenizer=None, stop_words=None, token_pattern=r'\b\w\w+\b', ngram_range=(1, 1), analyzer='word', max_df=1.0, min_df=1, max_features=None, vocabulary=None, binary=False)
# Importing the class NMF
from NMF import NMF

# creating an object of the class NMF, here nmfObject is the object of the class NMF from which functions present inside this class can be accessed.
nmfObject = NMF(df,bow[1],n_components=10, init=None, solver='cd', beta_loss='frobenius', tol=0.0001, max_iter=200, random_state=None, alpha=0.0, l1_ratio=0.0, verbose=0, shuffle=False)

# fit():
nmfModel = nmfObject.fit(bow[0])

# weightedMatrix():
matrixW = nmfObject.calculateWeightedMatrix()
print("**********Weighted Matrix after building NMF model**********")
print(matrixW)

# componentsMatrix():
matrixH = nmfObject.calculateComponentsMatrix()
print("**********Components Matrix after building NMF model**********")
print(matrixH)
from TopicVisualization import TopicVisualization
topicVisObj = TopicVisualization(nmfObject.nmf,nmfObject.n_components,nmfObject.matrix,nmfObject.vectorizer)

out = topicVisObj.generateWordCloudObject(numberOfTopWords=40,font_path=None, width=400, height=200, margin=2, ranks_only=None, prefer_horizontal=0.9, mask=None, scale=1, color_func=None, max_words=200, min_font_size=4, stopwords=None, random_state=None, background_color='black', max_font_size=None, font_step=1, mode='RGB', relative_scaling='auto', regexp=None, collocations=True, colormap=None, normalize_plurals=True, contour_width=0, contour_color='black', repeat=False)

topicVisObj.plotWordCloud(out)



