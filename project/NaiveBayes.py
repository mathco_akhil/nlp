from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import BernoulliNB
import pandas as pd
#import ModelEvaluationMetrices

class NaiveBayes:
    """
    This class builds a naïve bayes classifier and scores the dataset
    User has an option to choose the type of Naive Bayes classifier which are Multinomial, Gaussian or Bernoulli 
    Based on the selection the model is fit accordingly 

    """
    def __init__(self, df, type='Multinomial', alpha=1.0, fit_prior=True, class_prior=None,priors=None, var_smoothing=1e-09,binarize=0.0):
        """
        The constructor of the class creates an object for Logistic class
        Parameters:
        -----------
        
        df            : pandas dataframe (the dataset)
        type          : 'Multinomial' | 'Gaussian' | 'Bernoulli' 
        alpha         : float, optional (default=1.0) Additive (Laplace/Lidstone) smoothing parameter (0 for no smoothing).
        fit_prior     : boolean, optional (default=True) Whether to learn class prior probabilities or not. If false, a uniform prior will be used
        class_prior   : array-like, size (n_classes,), optional (default=None) Prior probabilities of the classes. If specified the priors are not adjusted according to the data. 
        priors        : array-like, shape (n_classes,) Prior probabilities of the classes. If specified the priors are not adjusted according to the data.
        var_smoothing : float, optional (default=1e-9) Portion of the largest variance of all features that is added to variances for calculation stability.
        binarize      : float or None, optional (default=0.0) Threshold for binarizing (mapping to booleans) of sample features. If None, input is presumed to already consist of binary vectors.
        
        Examples:
        ---------
        nvObject = NaiveBayes(df, type='Multinomial', alpha=1.0, fit_prior=True, class_prior=None,priors=None, var_smoothing=1e-09,binarize=0.0)
        
        """
        self.df = df
        self.type = type
        if type == 'Multinomial':
            self.clf = MultinomialNB(alpha=alpha, fit_prior=fit_prior, class_prior=class_prior)
            
        if type == 'Gaussian':
            self.clf = GaussianNB(priors=priors, var_smoothing=var_smoothing)
            
        if type == 'Bernoulli':
            self.clf = BernoulliNB(alpha=alpha, binarize=binarize, fit_prior=fit_prior, class_prior=class_prior)
            
            
            
    def fit(self,depVar,vector):
        """
        fit - This function fit the naive bayes model to the dataset.
        Parameters:
        -----------   
        depVar : Dependent variable from the dataset
        vector : bow[0], sparsed matrix which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the first element of the tuple is matrix, which is given as input over here. User can give matrix generated from other vectorization techniques.
                 For using vectorizeTfidf, give tfidf[0]
                 For using vectorizeHashing, give hashing_matrix
                 For using embedDoc2Vec, give doc2vecMatrix
            
        Examples:
        ---------
        nvModel = nvObject.fit(depVar='label',vector=bow[0])
        >>> Output  : Naive Bayes fitted to the dataset
        
        """
        self.vector = vector       
        if self.type == 'Gaussian':
            return self.clf.fit(self.vector.toarray(), self.df[depVar])
        else:
            return self.clf.fit(self.vector, self.df[depVar])
                      
            
    def scoreDataset(self,df):
        """
        scoreDataset - outputs the predicted probablities of the Naive Bayes model built, the user can input test/ train dataset in order to score the dataset
        Parameters:
        -----------   
        df : pandas dataframe (the dataset)
            
        Examples:
        ---------
        scoreDfNv = nvObject.scoreDataset(df)
        >>> Output : Dataframe with the predicted probablities
        """
        modelFit = self.clf
        vectorizeData = self.vector
        scoringTest = modelFit.predict_proba(vectorizeData)
        dataframe_test=pd.DataFrame(scoringTest, columns=['0','1'])
        scoringTest=dataframe_test['1']
        df_d = pd.concat([df, dataframe_test], axis=1)
        df_d = df_d.rename(columns={"1":"Pred_Prob"})
        df_d.to_csv("naivebayesout.csv",index=False)        
        return(df_d)

